package fr.keke142.ivernyaloadbalancer.configs;

import org.bukkit.configuration.Configuration;

public class PlayersCountsConfig {
    private int checkInterval;
    private int checkDays;

    public void load(Configuration config) {
        checkInterval = config.getInt("playersCounts.checkInterval", 1);
        checkDays = config.getInt("playersCounts.checkDays", 7);
    }

    public void save(Configuration config) {
        config.set("playersCounts.checkInterval", checkInterval);
        config.set("playersCounts.checkDays", checkDays);
    }

    public int getCheckInterval() {
        return checkInterval;
    }

    public int getCheckDays() {
        return checkDays;
    }
}
