package fr.keke142.ivernyaloadbalancer.configs;

import org.bukkit.configuration.Configuration;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class GroupsConfig {
    private final Map<String, Map<String, String>> groups = new HashMap<>();

    public void load(Configuration config) {
        for (String group : config.getConfigurationSection("groups").getKeys(false)) {
            for (String server : config.getConfigurationSection("groups." + group).getKeys(false)) {
                String world = config.getString("groups." + group + "." + server);

                if (groups.containsKey(group)) {
                    HashMap<String, String> servers = new HashMap<>(groups.get(group));

                    servers.put(server, world);

                    groups.put(group, servers);
                } else {
                    groups.put(group, Collections.singletonMap(server, world));
                }
            }
        }
    }

    public void save(Configuration config) {
        config.set("groups", groups);
    }

    public Map<String, Map<String, String>> getGroups() {
        return groups;
    }
}
