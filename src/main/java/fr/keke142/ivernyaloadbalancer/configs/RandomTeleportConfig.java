package fr.keke142.ivernyaloadbalancer.configs;

import org.bukkit.configuration.Configuration;

public class RandomTeleportConfig {
    private String defaultGroup;
    private int minRange;
    private int maxRange;
    private int cooldownSecs;

    public void load(Configuration config) {
        defaultGroup = config.getString("randomTeleport.defaultGroup", "epicsurvival");
        minRange = config.getInt("randomTeleport.minRange", 0);
        maxRange = config.getInt("randomTeleport.maxRange", 80000);
        cooldownSecs = config.getInt("randomTeleport.cooldownSecs", 14400);
    }

    public void save(Configuration config) {
        config.set("randomTeleport.defaultGroup", defaultGroup);
        config.set("randomTeleport.minRange", minRange);
        config.set("randomTeleport.maxRange", maxRange);
        config.set("randomTeleport.cooldownSecs", cooldownSecs);
    }

    public String getDefaultGroup() {
        return defaultGroup;
    }

    public int getMinRange() {
        return minRange;
    }

    public int getMaxRange() {
        return maxRange;
    }

    public int getCooldownSecs() {
        return cooldownSecs;
    }
}
