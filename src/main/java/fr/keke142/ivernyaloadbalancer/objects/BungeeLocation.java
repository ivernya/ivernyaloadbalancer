package fr.keke142.ivernyaloadbalancer.objects;

public class BungeeLocation {
    private String server;
    private String world;
    private double x;
    private double y;
    private double z;

    public BungeeLocation(String server, String world, double x, double y, double z) {
        this.server = server;
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public String getWorld() {
        return world;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public String getServer() {
        return server;
    }
}
