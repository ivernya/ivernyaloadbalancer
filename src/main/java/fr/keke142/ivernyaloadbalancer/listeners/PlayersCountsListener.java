package fr.keke142.ivernyaloadbalancer.listeners;

import com.gmail.tracebachi.DeltaRedis.Spigot.DeltaRedisMessageEvent;
import de.themoep.randomteleport.searcher.RandomSearcher;
import fr.keke142.ivernyaloadbalancer.IvernyaLoadBalancerPlugin;
import fr.keke142.ivernyaloadbalancer.configs.RandomTeleportConfig;
import fr.keke142.ivernyaloadbalancer.managers.MessageManager;
import io.papermc.lib.PaperLib;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

import static com.gmail.tracebachi.DeltaRedis.Shared.SplitPatterns.DELTA;

public class PlayersCountsListener implements Listener {
    private IvernyaLoadBalancerPlugin plugin;

    public PlayersCountsListener(IvernyaLoadBalancerPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    private void onLoadBalancingRequest(DeltaRedisMessageEvent event) {
        String channel = event.getChannel();
        String[] splitMessage = DELTA.split(event.getMessage());

        if (channel.equals("loadBalancingRequest")) {
            String playerUuid = splitMessage[0];
            String groupName = splitMessage[1];
            String worldName = splitMessage[2];

            new BukkitRunnable() {
                @Override
                public void run() {
                    Player player = Bukkit.getPlayer(UUID.fromString(playerUuid));

                    if (player != null) {
                        this.cancel();

                        plugin.getTpProtectionManager().addTpProtectionPlayer(player);

                        RandomTeleportConfig randomTeleportConfig = plugin.getConfigManager().getRandomTeleportConfig();

                        RandomSearcher searcher = plugin.getRandomTeleportAPI().getRandomSearcher(player, new Location(Bukkit.getWorld(worldName), 0, 0, 0), randomTeleportConfig.getMinRange(), randomTeleportConfig.getMaxRange());
                        searcher.search().thenApply(targetLoc -> {
                            searcher.getTargets().forEach(p -> {
                                Location belowLoc = targetLoc.clone().subtract(0, 1, 0);
                                Block belowBlock = belowLoc.getBlock();
                                ((Player) p).sendBlockChange(belowLoc, belowBlock.getType(), belowBlock.getData());

                                double finalX = targetLoc.getBlockX() + 0.5;
                                double finalY = targetLoc.getY() + 0.1;
                                double finalZ = targetLoc.getBlockZ() + 0.5;

                                targetLoc.setX(finalX);
                                targetLoc.setY(finalY);
                                targetLoc.setZ(finalZ);

                                PaperLib.teleportAsync(p, targetLoc).thenAccept((success) -> {
                                    plugin.getTpProtectionManager().removeTpProtectionPlayer(player);

                                    plugin.getLastLocationManager().setLastLocation(player, groupName);

                                    player.sendMessage(MessageManager.msg("loadbalancing.success", finalX, finalY, finalZ));
                                });
                            });
                            return true;
                        });
                    }
                }
            }.runTaskTimer(plugin, 1L, 1L);
        }
    }
}
