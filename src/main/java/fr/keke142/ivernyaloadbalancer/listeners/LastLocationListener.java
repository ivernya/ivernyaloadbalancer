package fr.keke142.ivernyaloadbalancer.listeners;

import com.gmail.tracebachi.DeltaRedis.Spigot.DeltaRedisMessageEvent;
import fr.keke142.ivernyaloadbalancer.IvernyaLoadBalancerPlugin;
import fr.keke142.ivernyaloadbalancer.managers.MessageManager;
import io.papermc.lib.PaperLib;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

import static com.gmail.tracebachi.DeltaRedis.Shared.SplitPatterns.DELTA;

public class LastLocationListener implements Listener {
    private IvernyaLoadBalancerPlugin plugin;

    public LastLocationListener(IvernyaLoadBalancerPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    private void onLoadBalancingRequest(DeltaRedisMessageEvent event) {
        String channel = event.getChannel();
        String[] splitMessage = DELTA.split(event.getMessage());

        if (channel.equals("lastLocationRequest")) {
            String playerUuid = splitMessage[0];
            String group = splitMessage[1];
            String locationWorld = splitMessage[2];
            double locationX = Double.parseDouble(splitMessage[3]);
            double locationY = Double.parseDouble(splitMessage[4]);
            double locationZ = Double.parseDouble(splitMessage[5]);

            new BukkitRunnable() {
                @Override
                public void run() {
                    Player player = Bukkit.getPlayer(UUID.fromString(playerUuid));
                    if (player != null) {
                        this.cancel();

                        plugin.getTpProtectionManager().addTpProtectionPlayer(player);

                        World world = Bukkit.getWorld(locationWorld);

                        if (world == null) {
                            plugin.getLastLocationManager().removeLastLocation(player, group);
                            plugin.getPlayersCountsManager().sendToLowestAverageGroup(player, group);
                        } else {
                            Location lastLocation = new Location(world, locationX, locationY, locationZ);
                            PaperLib.teleportAsync(player, lastLocation).thenAccept((result) -> {
                                plugin.getTpProtectionManager().removeTpProtectionPlayer(player);

                                int remainingSecs = plugin.getLastLocationManager().getLocationRemainingSecs(player, group);

                                long hours = remainingSecs / 3600;
                                long minutes = (remainingSecs % 3600) / 60;
                                long seconds = remainingSecs % 60;

                                String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);

                                player.sendMessage(MessageManager.msg("loadbalancing.lastLocation", timeString));
                            });
                        }
                    }
                }
            }.runTaskTimer(plugin, 1L, 1L);
        }
    }
}
