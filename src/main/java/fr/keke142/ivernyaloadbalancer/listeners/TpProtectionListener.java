package fr.keke142.ivernyaloadbalancer.listeners;

import fr.keke142.ivernyaloadbalancer.IvernyaLoadBalancerPlugin;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class TpProtectionListener implements Listener {
    private final IvernyaLoadBalancerPlugin plugin;

    public TpProtectionListener(IvernyaLoadBalancerPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player)) return;

        Player p = (Player) e.getEntity();

        if (plugin.getTpProtectionManager().isPlayerTpProtected(p)) {
            e.setCancelled(true);
        }
    }


    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();

        if (plugin.getTpProtectionManager().isPlayerTpProtected(p)) {
            e.setCancelled(true);
        }
    }


    @EventHandler
    public void onPlayerPortal(PlayerPortalEvent e) {
        Player p = e.getPlayer();

        if (plugin.getTpProtectionManager().isPlayerTpProtected(p)) {
            e.setCancelled(true);
        }
    }


    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();

        if (plugin.getTpProtectionManager().isPlayerTpProtected(p)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        plugin.getTpProtectionManager().removeTpProtectionPlayer(e.getPlayer());
    }
}
