package fr.keke142.ivernyaloadbalancer.database;

import fr.keke142.ivernyaloadbalancer.managers.DatabaseManager;
import fr.keke142.ivernyaloadbalancer.objects.BungeeLocation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class LastLocationTable extends AbstractTable {
    private static final String SET_LAST_LOCATION = "INSERT INTO ivlb_lastlocation (player, `group`, server, world, x, y, z) VALUES(?,?,?,?,?,?,?)";
    private static final String REMOVE_LAST_LOCATION = "DELETE FROM ivlb_lastlocation WHERE player=? AND `group`=?";
    private static final String GET_LAST_LOCATION = "SELECT server, world, x, y, z FROM ivlb_lastlocation WHERE player=? AND `group`=?";
    private static final String GET_LOCATION_DATE = "SELECT locationDate FROM ivlb_lastlocation WHERE player=? AND `group`=?";

    public LastLocationTable(DatabaseManager databaseManager) {
        super(databaseManager);
    }


    public void setLastLocation(String playerUuid, String group, String server, String world, double x, double y, double z) {
        try (Connection connection = getDatabaseManager().getHikari().getConnection();
             PreparedStatement setLastLocation = connection.prepareStatement(SET_LAST_LOCATION)) {

            setLastLocation.setString(1, playerUuid);
            setLastLocation.setString(2, group);
            setLastLocation.setString(3, server);
            setLastLocation.setString(4, world);
            setLastLocation.setDouble(5, x);
            setLastLocation.setDouble(6, y);
            setLastLocation.setDouble(7, z);

            setLastLocation.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeLastLocation(String playerUuid, String group) {
        try (Connection connection = getDatabaseManager().getHikari().getConnection();
             PreparedStatement removeLastLocation = connection.prepareStatement(REMOVE_LAST_LOCATION)) {

            removeLastLocation.setString(1, playerUuid);
            removeLastLocation.setString(2, group);

            removeLastLocation.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public BungeeLocation getLastLocation(String playerUuid, String group) {
        try (Connection connection = getDatabaseManager().getHikari().getConnection();
             PreparedStatement getLastLocation = connection.prepareStatement(GET_LAST_LOCATION)) {

            getLastLocation.setString(1, playerUuid);
            getLastLocation.setString(2, group);

            ResultSet res = getLastLocation.executeQuery();
            if (res.next()) {
                String server = res.getString(1);
                String world = res.getString(2);
                double x = res.getDouble(3);
                double y = res.getDouble(4);
                double z = res.getDouble(5);

                return new BungeeLocation(server, world, x, y, z);
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Date getLocationDate(String playerUuid, String group) {
        try (Connection connection = getDatabaseManager().getHikari().getConnection();
             PreparedStatement getLocationDate = connection.prepareStatement(GET_LOCATION_DATE)) {

            getLocationDate.setString(1, playerUuid);
            getLocationDate.setString(2, group);

            ResultSet res = getLocationDate.executeQuery();
            if (res.next()) {
                return res.getTimestamp(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public String[] getTable() {
        return new String[]{"ivlb_lastlocation", "`player` VARCHAR(36) NOT NULL, " +
                "`group` VARCHAR(36) NOT NULL, " +
                "`server` VARCHAR(36) NOT NULL, " +
                "`world` VARCHAR(36) NOT NULL, " +
                "`x` DOUBLE NOT NULL, " +
                "`y` DOUBLE NOT NULL, " +
                "`z` DOUBLE NOT NULL, " +
                "`locationDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, " +
                "PRIMARY KEY (`player`, `group`)",
                "ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci"};
    }
}
