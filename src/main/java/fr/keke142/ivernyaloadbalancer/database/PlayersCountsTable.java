package fr.keke142.ivernyaloadbalancer.database;

import fr.keke142.ivernyaloadbalancer.IvernyaLoadBalancerPlugin;
import fr.keke142.ivernyaloadbalancer.managers.DatabaseManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PlayersCountsTable extends AbstractTable {
    private static final String ADD_PLAYERS_COUNT = "INSERT INTO ivlb_playerscounts (serverName, playersCount) VALUES(?,?)";
    private static final String GET_PLAYERS_COUNTS = "SELECT AVG(playersCount) averagePlayersCount FROM ivlb_playerscounts WHERE serverName=? AND (`checkTime` > DATE_SUB(now(), INTERVAL " + IvernyaLoadBalancerPlugin.getInstance().getConfigManager().getPlayersCountsConfig().getCheckDays() + "  DAY))";

    public PlayersCountsTable(DatabaseManager databaseManager) {
        super(databaseManager);
    }

    public void addPlayersCount(String serverName, int playersCount) {
        try (Connection connection = getDatabaseManager().getHikari().getConnection();
             PreparedStatement addPlayersCount = connection.prepareStatement(ADD_PLAYERS_COUNT)) {

            addPlayersCount.setString(1, serverName);
            addPlayersCount.setInt(2, playersCount);

            addPlayersCount.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public double getPlayersCount(String serverName) {
        try (Connection connection = getDatabaseManager().getHikari().getConnection();
             PreparedStatement getPlayersCounts = connection.prepareStatement(GET_PLAYERS_COUNTS)) {

            getPlayersCounts.setString(1, serverName);

            ResultSet res = getPlayersCounts.executeQuery();
            while (res.next()) {
                return res.getDouble(1);
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public String[] getTable() {
        return new String[]{"ivlb_playerscounts", "`serverName` VARCHAR(36) NOT NULL, " +
                "`playersCount` INT NOT NULL, " +
                "`checkTime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP," +
                "PRIMARY KEY (`serverName`, `playersCount`, `checkTime`)",
                "ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci"};
    }
}
