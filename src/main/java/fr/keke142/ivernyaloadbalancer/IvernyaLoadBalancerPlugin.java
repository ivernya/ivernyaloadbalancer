package fr.keke142.ivernyaloadbalancer;

import de.themoep.randomteleport.RandomTeleport;
import de.themoep.randomteleport.api.RandomTeleportAPI;
import fr.keke142.ivernyaloadbalancer.commands.LoadBalancingCommand;
import fr.keke142.ivernyaloadbalancer.commands.LoadBalancingStatusCommand;
import fr.keke142.ivernyaloadbalancer.commands.RandomTeleportCommand;
import fr.keke142.ivernyaloadbalancer.listeners.LastLocationListener;
import fr.keke142.ivernyaloadbalancer.listeners.PlayersCountsListener;
import fr.keke142.ivernyaloadbalancer.listeners.TpProtectionListener;
import fr.keke142.ivernyaloadbalancer.managers.*;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.util.Locale;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

public class IvernyaLoadBalancerPlugin extends JavaPlugin {
    private static IvernyaLoadBalancerPlugin instance;

    private ConfigManager configManager;
    private DatabaseManager databaseManager;
    private RandomTeleportAPI randomTeleportAPI;
    private PlayersCountsManager playersCountsManager;
    private LastLocationManager lastLocationManager;
    private TpProtectionManager tpProtectionManager;
    private LoadBalancingManager loadBalancingManager;

    public static IvernyaLoadBalancerPlugin getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;

        configManager = new ConfigManager(this);
        databaseManager = new DatabaseManager(this);

        databaseManager.loadDatabase();

        MessageManager.loadLocale(this, Locale.FRENCH);

        playersCountsManager = new PlayersCountsManager(this);
        tpProtectionManager = new TpProtectionManager();
        lastLocationManager = new LastLocationManager(this);
        loadBalancingManager = new LoadBalancingManager(this);

        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(new PlayersCountsListener(this), this);
        pluginManager.registerEvents(new LastLocationListener(this), this);
        pluginManager.registerEvents(new TpProtectionListener(this), this);

        this.getCommand("loadbalancing").setExecutor(new LoadBalancingCommand(this));
        this.getCommand("loadbalancingstatus").setExecutor(new LoadBalancingStatusCommand(this));
        this.getCommand("randomteleport").setExecutor(new RandomTeleportCommand(this));

        if (getServer().getPluginManager().getPlugin("RandomTeleport") != null) {
            randomTeleportAPI = (RandomTeleport) getServer().getPluginManager().getPlugin("RandomTeleport");
        } else {

        }

        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public RandomTeleportAPI getRandomTeleportAPI() {
        return randomTeleportAPI;
    }

    public PlayersCountsManager getPlayersCountsManager() {
        return playersCountsManager;
    }

    public LastLocationManager getLastLocationManager() {
        return lastLocationManager;
    }

    public TpProtectionManager getTpProtectionManager() {
        return tpProtectionManager;
    }

    public LoadBalancingManager getLoadBalancingManager() {
        return loadBalancingManager;
    }

    /**
     * Create a default configuration file from the .jar.
     *
     * @param actual      The destination file
     * @param defaultName The name of the file inside the jar's defaults folder
     */
    public void createDefaultConfiguration(File actual, String defaultName) {

        // Make parent directories
        File parent = actual.getParentFile();
        if (!parent.exists()) {
            parent.mkdirs();
        }

        if (actual.exists()) {
            return;
        }

        JarFile file = null;
        InputStream input = null;
        try {
            file = new JarFile(getFile());
            ZipEntry copy = file.getEntry(defaultName);
            if (copy == null) {
                file.close();
                throw new FileNotFoundException();
            }
            input = file.getInputStream(copy);
        } catch (IOException e) {
            getLogger().severe("Unable to read default configuration: " + defaultName);
        }

        if (input != null) {
            FileOutputStream output = null;

            try {
                output = new FileOutputStream(actual);
                byte[] buf = new byte[8192];
                int length;
                while ((length = input.read(buf)) > 0) {
                    output.write(buf, 0, length);
                }

                getLogger().info("Default configuration file written: " + actual.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    input.close();
                } catch (IOException ignore) {
                }

                try {
                    if (output != null) {
                        output.close();
                    }
                } catch (IOException ignore) {
                }
            }
        }
        if (file != null) {
            try {
                file.close();
            } catch (IOException ignore) {
            }
        }
    }
}
