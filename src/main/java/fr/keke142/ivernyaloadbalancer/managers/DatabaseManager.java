package fr.keke142.ivernyaloadbalancer.managers;

import com.zaxxer.hikari.HikariDataSource;
import fr.keke142.ivernyaloadbalancer.IvernyaLoadBalancerPlugin;
import fr.keke142.ivernyaloadbalancer.database.LastLocationTable;
import fr.keke142.ivernyaloadbalancer.database.PlayersCountsTable;

public class DatabaseManager {
    private IvernyaLoadBalancerPlugin plugin;
    private PlayersCountsTable playersCountsTable;
    private LastLocationTable lastLocationTable;

    private HikariDataSource hikari;

    public DatabaseManager(IvernyaLoadBalancerPlugin plugin) {
        this.plugin = plugin;
    }

    public void loadDatabase() {

        hikari = new HikariDataSource();
        hikari.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        hikari.addDataSourceProperty("serverName", plugin.getConfigManager().getDatabaseConfig().getHost());
        hikari.addDataSourceProperty("port", plugin.getConfigManager().getDatabaseConfig().getPort());
        hikari.addDataSourceProperty("databaseName", plugin.getConfigManager().getDatabaseConfig().getDatabase());
        hikari.addDataSourceProperty("user", plugin.getConfigManager().getDatabaseConfig().getUserName());
        hikari.addDataSourceProperty("password", plugin.getConfigManager().getDatabaseConfig().getPassword());

        playersCountsTable = new PlayersCountsTable(this);
        playersCountsTable.createTable();

        lastLocationTable = new LastLocationTable(this);
        lastLocationTable.createTable();
    }

    public HikariDataSource getHikari() {
        return hikari;
    }

    public PlayersCountsTable getPlayersCountsTable() {
        return playersCountsTable;
    }

    public LastLocationTable getLastLocationTable() {
        return lastLocationTable;
    }
}
