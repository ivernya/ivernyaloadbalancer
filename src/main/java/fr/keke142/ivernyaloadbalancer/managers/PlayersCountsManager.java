package fr.keke142.ivernyaloadbalancer.managers;

import com.gmail.tracebachi.DeltaRedis.Spigot.DeltaRedisApi;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import fr.keke142.ivernyaloadbalancer.IvernyaLoadBalancerPlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlayersCountsManager {
    private final IvernyaLoadBalancerPlugin plugin;
    private final DatabaseManager databaseManager;

    public PlayersCountsManager(IvernyaLoadBalancerPlugin plugin) {
        this.plugin = plugin;
        databaseManager = plugin.getDatabaseManager();

        new BukkitRunnable() {
            @Override
            public void run() {
                DeltaRedisApi deltaApi = DeltaRedisApi.instance();

                addPlayersCount(deltaApi.getServerName(), Bukkit.getOnlinePlayers().size());
            }
        }.runTaskTimer(plugin, 0, plugin.getConfigManager().getPlayersCountsConfig().getCheckInterval() * 20);
    }

    public void addPlayersCount(String serverName, int playersCount) {
        new BukkitRunnable() {
            @Override
            public void run() {
                databaseManager.getPlayersCountsTable().addPlayersCount(serverName, playersCount);
            }

        }.runTaskAsynchronously(plugin);
    }

    public double getPlayersCount(String serverName) {
        return databaseManager.getPlayersCountsTable().getPlayersCount(serverName);
    }

    public void sendToLowestAverageGroup(Player player, String groupName) {
        new BukkitRunnable() {
            @Override
            public void run() {
                List<String> servers = new ArrayList<>(plugin.getConfigManager().getGroupsConfig().getGroups().get(groupName).keySet());
                DeltaRedisApi deltaApi = DeltaRedisApi.instance();

                Map<String, Double> serversCounts = new HashMap<>();
                for (String server : servers) {
                    serversCounts.put(server, getPlayersCount(server));
                }


                deltaApi.findPlayer(player.getName(), (cachedPlayer -> {

                    Map.Entry<String, Double> min = null;
                    for (Map.Entry<String, Double> entry : serversCounts.entrySet()) {
                        if (min == null || min.getValue() > entry.getValue()) {
                            min = entry;
                        }
                    }

                    String serverToSend = min.getKey();
                    String currentServer = cachedPlayer.getServer();

                    if (!serverToSend.equalsIgnoreCase(currentServer)) {
                        ByteArrayDataOutput out = ByteStreams.newDataOutput();
                        out.writeUTF("Connect");
                        out.writeUTF(serverToSend);

                        player.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
                    }

                    String worldToSend = plugin.getConfigManager().getGroupsConfig().getGroups().get(groupName).get(serverToSend);

                    deltaApi.publish(serverToSend, "loadBalancingRequest", player.getUniqueId().toString(), groupName, worldToSend);

                }));

            }

        }.runTaskAsynchronously(plugin);

    }

    public boolean doesGroupExists(String groupName) {
        return plugin.getConfigManager().getGroupsConfig().getGroups().containsKey(groupName);
    }

    public int howManyServersInThisGroup(String groupName) {
        return plugin.getConfigManager().getGroupsConfig().getGroups().get(groupName).size();
    }

}
