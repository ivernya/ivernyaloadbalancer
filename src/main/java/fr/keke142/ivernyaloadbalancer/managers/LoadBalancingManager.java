package fr.keke142.ivernyaloadbalancer.managers;

import fr.keke142.ivernyaloadbalancer.IvernyaLoadBalancerPlugin;
import fr.keke142.ivernyaloadbalancer.objects.BungeeLocation;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LoadBalancingManager {
    private final IvernyaLoadBalancerPlugin plugin;

    public LoadBalancingManager(IvernyaLoadBalancerPlugin plugin) {
        this.plugin = plugin;
    }

    public boolean loadBalance(CommandSender sender, Player target, String groupName) {
        PlayersCountsManager playersCountsManager = plugin.getPlayersCountsManager();

        if (!playersCountsManager.doesGroupExists(groupName)) {
            sender.sendMessage(MessageManager.msg("loadbalancing.cannotFindThisGroup"));
            return false;
        }

        if (playersCountsManager.howManyServersInThisGroup(groupName) < 1) {
            sender.sendMessage(MessageManager.msg("loadbalancing.notEnoughServersInGroup"));
            return false;
        }

        LastLocationManager lastLocationManager = plugin.getLastLocationManager();

        BungeeLocation lastLocation = lastLocationManager.getLastLocation(target, groupName);

        if (lastLocation == null) {
            playersCountsManager.sendToLowestAverageGroup(target, groupName);
        } else {
            int secondsDifference = lastLocationManager.getLocationRemainingSecs(target, groupName);

            if (secondsDifference <= 0) {
                lastLocationManager.removeLastLocation(target, groupName);

                playersCountsManager.sendToLowestAverageGroup(target, groupName);
            } else {
                lastLocationManager.teleportToLastLocation(target, groupName);
            }
        }

        return true;
    }
}
