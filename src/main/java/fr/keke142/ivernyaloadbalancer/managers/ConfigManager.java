package fr.keke142.ivernyaloadbalancer.managers;

import fr.keke142.ivernyaloadbalancer.IvernyaLoadBalancerPlugin;
import fr.keke142.ivernyaloadbalancer.configs.DatabaseConfig;
import fr.keke142.ivernyaloadbalancer.configs.GroupsConfig;
import fr.keke142.ivernyaloadbalancer.configs.PlayersCountsConfig;
import fr.keke142.ivernyaloadbalancer.configs.RandomTeleportConfig;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.io.IOException;

public class ConfigManager {
    private IvernyaLoadBalancerPlugin plugin;
    private DatabaseConfig database = new DatabaseConfig();
    private PlayersCountsConfig playersCountsConfig = new PlayersCountsConfig();
    private GroupsConfig groupsConfig = new GroupsConfig();
    private RandomTeleportConfig randomTeleportConfig = new RandomTeleportConfig();

    private File file;
    private FileConfiguration config;

    public ConfigManager(IvernyaLoadBalancerPlugin plugin) {
        this.plugin = plugin;
        file = new File(plugin.getDataFolder(), "config.yml");
        plugin.createDefaultConfiguration(file, "config.yml");

        config = plugin.getConfig();
        config.options().copyDefaults(true);
        plugin.saveDefaultConfig();

        database.load(config);
        playersCountsConfig.load(config);
        groupsConfig.load(config);
        randomTeleportConfig.load(config);
    }

    public void save() {
        try {
            database.save(config);
            playersCountsConfig.save(config);
            groupsConfig.save(config);
            randomTeleportConfig.save(config);

            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void reload() {
        config = plugin.getConfig();
        config.options().copyDefaults(true);
        plugin.saveDefaultConfig();

        database.load(config);
        playersCountsConfig.load(config);
        groupsConfig.load(config);
        randomTeleportConfig.load(config);
    }

    public DatabaseConfig getDatabaseConfig() {
        return database;
    }

    public PlayersCountsConfig getPlayersCountsConfig() {
        return playersCountsConfig;
    }

    public GroupsConfig getGroupsConfig() {
        return groupsConfig;
    }

    public RandomTeleportConfig getRandomTeleportConfig() {
        return randomTeleportConfig;
    }
}
