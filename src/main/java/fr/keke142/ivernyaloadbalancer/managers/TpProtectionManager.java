package fr.keke142.ivernyaloadbalancer.managers;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class TpProtectionManager {
    private final List<String> tpProtectionPlayers;

    public TpProtectionManager() {
        this.tpProtectionPlayers = new ArrayList<>();
    }

    public void addTpProtectionPlayer(Player p) {
        tpProtectionPlayers.add(p.getUniqueId().toString());
    }

    public void removeTpProtectionPlayer(Player p) {
        p.setFireTicks(0);
        tpProtectionPlayers.remove(p.getUniqueId().toString());
    }

    public boolean isPlayerTpProtected(Player p) {
        return tpProtectionPlayers.contains(p.getUniqueId().toString());
    }
}
