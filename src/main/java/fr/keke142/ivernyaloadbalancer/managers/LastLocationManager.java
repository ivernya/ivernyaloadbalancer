package fr.keke142.ivernyaloadbalancer.managers;

import com.gmail.tracebachi.DeltaRedis.Spigot.DeltaRedisApi;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import fr.keke142.ivernyaloadbalancer.IvernyaLoadBalancerPlugin;
import fr.keke142.ivernyaloadbalancer.objects.BungeeLocation;
import fr.keke142.ivernyaloadbalancer.utils.DateUtil;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.Date;

public class LastLocationManager {
    private final IvernyaLoadBalancerPlugin plugin;

    public LastLocationManager(IvernyaLoadBalancerPlugin plugin) {
        this.plugin = plugin;
    }

    public void setLastLocation(Player player, String group) {
        DeltaRedisApi deltaApi = DeltaRedisApi.instance();

        Location location = player.getLocation();

        plugin.getDatabaseManager().getLastLocationTable().setLastLocation(player.getUniqueId().toString(), group, deltaApi.getServerName(), location.getWorld().getName(), location.getX(), location.getY(), location.getZ());
    }

    public void removeLastLocation(Player player, String group) {
        plugin.getDatabaseManager().getLastLocationTable().removeLastLocation(player.getUniqueId().toString(), group);
    }

    public BungeeLocation getLastLocation(Player player, String group) {
        return plugin.getDatabaseManager().getLastLocationTable().getLastLocation(player.getUniqueId().toString(), group);
    }

    public void teleportToLastLocation(Player player, String group) {
        DeltaRedisApi deltaApi = DeltaRedisApi.instance();

        BungeeLocation lastLocation = getLastLocation(player, group);

        String serverToSend = lastLocation.getServer();

        deltaApi.findPlayer(player.getName(), (cachedPlayer -> {
            String currentServer = cachedPlayer.getServer();

            if (!serverToSend.equalsIgnoreCase(currentServer)) {
                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("Connect");
                out.writeUTF(serverToSend);

                player.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
            }

            String locationXStr = String.valueOf(lastLocation.getX());
            String locationYStr = String.valueOf(lastLocation.getY());
            String locationZStr = String.valueOf(lastLocation.getZ());

            deltaApi.publish(serverToSend, "lastLocationRequest", player.getUniqueId().toString(), group, lastLocation.getWorld(), locationXStr, locationYStr, locationZStr);

        }));

    }

    public Date getLocationDate(Player player, String group) {
        return plugin.getDatabaseManager().getLastLocationTable().getLocationDate(player.getUniqueId().toString(), group);
    }

    public int getLocationRemainingSecs(Player player, String group) {
        Date locationDate = getLocationDate(player, group);

        Date dateTimeCanVote = DateUtil.addSecondsToDate(locationDate, plugin.getConfigManager().getRandomTeleportConfig().getCooldownSecs());

        Date currentDate = new Date();

        return (int) ((dateTimeCanVote.getTime() - currentDate.getTime()) / 1000);
    }
}
