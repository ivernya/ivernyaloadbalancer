package fr.keke142.ivernyaloadbalancer.commands;

import fr.keke142.ivernyaloadbalancer.IvernyaLoadBalancerPlugin;
import fr.keke142.ivernyaloadbalancer.managers.MessageManager;
import fr.keke142.ivernyaloadbalancer.managers.PlayersCountsManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Map;

public class LoadBalancingStatusCommand implements CommandExecutor {
    private final IvernyaLoadBalancerPlugin plugin;

    public LoadBalancingStatusCommand(IvernyaLoadBalancerPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("iv.loadbalancing")) {
            sender.sendMessage(MessageManager.msg("commands.noPermission"));
            return true;
        }

        if (args.length < 1) {
            sender.sendMessage(MessageManager.msg("commands.mustSpecifyGroup"));
            return true;
        }

        String groupName = args[0];

        PlayersCountsManager playersCountsManager = plugin.getPlayersCountsManager();

        if (!playersCountsManager.doesGroupExists(groupName)) {
            sender.sendMessage(MessageManager.msg("loadbalancing.cannotFindThisGroup"));
            return true;
        }

        if (playersCountsManager.howManyServersInThisGroup(groupName) < 1) {
            sender.sendMessage(MessageManager.msg("loadbalancing.notEnoughServersInGroup"));
            return true;
        }

        new BukkitRunnable() {
            @Override
            public void run() {
                Map<String, String> servers = plugin.getConfigManager().getGroupsConfig().getGroups().get(groupName);

                servers.forEach((server, worlds) -> {
                    sender.sendMessage(ChatColor.GOLD + "The average players count for the server " + server + " is " + plugin.getPlayersCountsManager().getPlayersCount(server));
                });
            }

        }.runTaskAsynchronously(plugin);

        return true;
    }
}
