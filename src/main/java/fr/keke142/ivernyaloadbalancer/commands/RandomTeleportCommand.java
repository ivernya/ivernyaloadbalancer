package fr.keke142.ivernyaloadbalancer.commands;

import fr.keke142.ivernyaloadbalancer.IvernyaLoadBalancerPlugin;
import fr.keke142.ivernyaloadbalancer.managers.MessageManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RandomTeleportCommand implements CommandExecutor {
    private final IvernyaLoadBalancerPlugin plugin;

    public RandomTeleportCommand(IvernyaLoadBalancerPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("iv.randomteleport")) {
            sender.sendMessage(MessageManager.msg("commands.noPermission"));
            return true;
        }

        Player p = (Player) sender;

        String groupName = plugin.getConfigManager().getRandomTeleportConfig().getDefaultGroup();

        plugin.getLoadBalancingManager().loadBalance(p, p, groupName);

        return true;
    }
}
