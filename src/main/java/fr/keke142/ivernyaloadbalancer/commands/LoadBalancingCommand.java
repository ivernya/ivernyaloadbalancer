package fr.keke142.ivernyaloadbalancer.commands;

import fr.keke142.ivernyaloadbalancer.IvernyaLoadBalancerPlugin;
import fr.keke142.ivernyaloadbalancer.managers.MessageManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LoadBalancingCommand implements CommandExecutor {
    private IvernyaLoadBalancerPlugin plugin;

    public LoadBalancingCommand(IvernyaLoadBalancerPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("iv.loadbalancing")) {
            sender.sendMessage(MessageManager.msg("commands.noPermission"));
            return true;
        }

        if (args.length < 1) {
            sender.sendMessage(MessageManager.msg("commands.mustSpecifyGroup"));
            return true;
        }

        String groupName = args[0];

        if (args.length < 2) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(MessageManager.msg("commands.playerOnly"));
                return true;
            }

            Player player = (Player) sender;

            plugin.getLoadBalancingManager().loadBalance(player, player, groupName);
        } else {
            String targetName = args[1];

            Player targetPlayer = Bukkit.getPlayer(targetName);

            if (targetPlayer == null) {
                sender.sendMessage(MessageManager.msg("commands.unknownPlayer"));
                return true;
            }

            plugin.getLoadBalancingManager().loadBalance(sender, targetPlayer, groupName);
        }

        return true;
    }
}
